package test.com.slice.regexnumber;

import main.com.slice.regexhexnumber.RegexHexNumber;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class RegexHexNumberTest {

    RegexHexNumber cut = new RegexHexNumber();

    static Arguments[] regexHexNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments(true, "FA45"),
                Arguments.arguments(false, "BSSUXD"),
                Arguments.arguments(true, "0000"),
                Arguments.arguments(false, "Alona"),
                Arguments.arguments(true, "12af")


        };
    }

    @ParameterizedTest
    @MethodSource("regexHexNumberTestArgs")
    void RegexHexNumberTest(boolean expected, String hexNumber){
        boolean actual = cut.checkHexNumber(hexNumber);
        Assertions.assertEquals(expected, actual);
    }


}
