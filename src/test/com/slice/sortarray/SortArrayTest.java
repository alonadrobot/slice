package test.com.slice.sortarray;

import main.com.slice.sortarray.SortArray;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class SortArrayTest {

    SortArray cut = new SortArray();

    static Arguments[] sortTestArgs() {
        return new Arguments[]{
                Arguments.arguments("56789", new int[]{9, 6, 7, 8, 5}),
                Arguments.arguments("12345", new int[]{3, 5, 4, 1, 2}),
        };
    }

    @ParameterizedTest
    @MethodSource("sortTestArgs")
    void sortBubble(String expected, int[] array) {
        String actual = cut.array1(array);
        Assertions.assertEquals(expected, actual);

    }
}

