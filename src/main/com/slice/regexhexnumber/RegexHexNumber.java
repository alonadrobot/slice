package main.com.slice.regexhexnumber;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexHexNumber {
    public static void main(String[] args) {
        System.out.println(checkHexNumber("0000"));
    }

    public static boolean checkHexNumber(String str){

        Pattern pattern = Pattern.compile("[^0-9A-Fa-f]");

        if (pattern.matcher(str).find()){
            return false;
        }
        else {
            return true;
        }
    }

}
