package main.com.slice.sortarray;

public class SortArray {

    public String array1(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 1 + i; j < array.length; j++) {
                int temp = array[i];
                if (temp > array[j]) {
                    int x = array[j];
                    array[i] = x;
                    array[j] = temp;
                }
            }
        }
        String rev = "";
        for (int i = 0; i < array.length; i++) {
            rev = rev + array[i];
        }
        return rev;
    }
}

